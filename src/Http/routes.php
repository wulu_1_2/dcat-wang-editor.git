<?php

use Hkw\WangEditor\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('wang-editor' , Controllers\WangEditorController::class . '@index');
Route::post('/editor/wang/image' , [Controllers\WangEditorController::class , 'editorImage']);
Route::post('/editor/wang/video' , [Controllers\WangEditorController::class , 'editorVideo']);