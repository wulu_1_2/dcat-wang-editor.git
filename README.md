# Dcat Admin WangEditor扩展（自用）
### 背景：找遍各种富文本编辑器，没有一个能满足我的需求，我的需求如下：
1. 支持图文、支持视频，其它文字排版功能差不多就行了
2. 支持云直传（如七牛云直传,OSS直传）
3. 大文件分片上传，如果文件超过1M自动使用分片上传，主要用于视频上传

### 功能说明
1. 集成了wangEditor5 版本，界面清爽美观
2. 集成了七牛云、阿里云OSS
3. 支持文件直传，视频分片上传

####前置包
1. "dcat/laravel-admin": "~2.0",
2. "zgldh/qiniu-laravel-storage": "0.10.4"（使用七牛云需要）
3. "bmslaravel/aliyun-sts": "^1.1" （使用阿里云oss需要）

#### 七牛云：在filesystem.php 的 disks下增加下面代码，当默认文件驱动为qiniu时自动开启前端直传和大文件分片上传

```php
'qiniu' => [
    'driver'                 => 'qiniu',
    'domains'                => [
        'default' => env('QINIU_DOMAIN_DEFAULT'), //你的七牛域名
        'https'   => env('QINIU_DOMAIN_HTTPS'),         //你的HTTPS域名
        'custom'  => env('QINIU_DOMAIN_HTTPS'),                //Useless 没啥用，请直接使用上面的 default 项
    ],
    'access_key'             => env('QINIU_ACCESS_KEY'),  //AccessKey
    'secret_key'             => env('QINIU_SECRET_KEY'),  //SecretKey
    'bucket'                 => env('QINIU_BUCKET'),  //Bucket名字
    'notify_url'             => '',  //持久化处理回调地址
    'access'                 => 'public',  //空间访问控制 public 或 private
    'hotlink_prevention_key' => '', // CDN 时间戳防盗链的 key。 设置为 null 则不启用本功能。
    'region'                 => env('QINIU_REGION', 'z1')
],
```
####  阿里云oss： 安装bmslaravel/aliyun-sts扩展后发布配置文件sts.php，完善配置文件（里面的配置项需要进阿里云配置RAM授权策略，具体参考阿里云OSS直传手册），当默认文件驱动为oss时自动开启前端直传和大文件分片上传

### 使用方法

```php
$form->wang('detail');
$form->wang('detail')->height(800);//设置高度
$form->wang('detail')->zIndex(99999);//设置z-index层级
```



